package Operations;

public interface InterfaceChanges {
    default void defaultMethod(){
        System.out.println("Hello from a default method from the interface!");
    }
    static void staticMethod(){
        System.out.println("Hello from a static method from the interface!");
    }
}
