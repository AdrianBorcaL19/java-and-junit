
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) throws IOException {
        //HandlingExceptions test1 = new HandlingExceptions();
        //test1.testException();
        //Time test2 = new Time();
        //test2.testTime();
        //LambdaMRFI test3 = new LambdaMRFI();
        //test3.testLambda();
        //test3.testmethodReference();
        //test3.testFunctionalInterfaces();
        //Streams test4 = new Streams();
        //test4.collectGreaterThanFive();
        //InterfaceChangesImpl test5 = new InterfaceChangesImpl();
        //test5.defaultMethod();
        //InterfaceChanges.staticMethod();
        //OptionalTest test6 = new OptionalTest();
        //test6.testOptional();
        //FactoryMethods test7 = new FactoryMethods();
        //test7.testFactoryMethods();
        //TryWithResources test8 = new TryWithResources();
        //test8.testTryWithResources();
        //ReadFile test9 = new ReadFile();
        //test9.firstMethod();
        //test9.secondMethod();
        //StringMethods test10 = new StringMethods();
        //test10.removeBeginningWhitespace("     aaa    a ");
        //test10.removeEndWhitespace("     aaa   a  ");
        //test10.removeWhitespace("     aaa     ");
        logger.trace("We've just greeted the user!");
        logger.debug("We've just greeted the user!");
        logger.info("We've just greeted the user!");
        logger.warn("We've just greeted the user!");
        logger.error("We've just greeted the user!");
        logger.fatal("We've just greeted the user!");
    }
}
