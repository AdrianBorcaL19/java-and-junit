package TestClasses;

import Operations.FInterfaces;
import Operations.OperationClass;
import Operations.OperationInterface;

import java.util.function.BiFunction;
import java.util.function.Function;

public class LambdaMRFI {
    public void testLambda(){
        OperationInterface sum = (int x, int y)-> x + y;
        OperationInterface difference = (int x, int y) -> x - y;
        System.out.println("Sum with Lambda: 10 + 10= " + sum.operation(10,10));
        System.out.println("Difference with Lambda: 10 - 5= " + difference.operation(10,5));
    }
    public void testmethodReference(){
        OperationClass op = new OperationClass();
        BiFunction<Integer,Integer,Integer> sum = op::add;
        System.out.println("Sum with Method Reference: 10+10= "+ sum.apply(10,10));
        Function<Integer,Integer> square = op::square;
        System.out.println("Square with Method Reference: 10 ^ 2= "+ square.apply(10));
    }
    public void testFunctionalInterfaces(){
        FInterfaces operation1 = (String x, String y)->x+y;
        System.out.println("Concatenate using functional interface "+ operation1.concatenate("Code","mart"));
    }
}
