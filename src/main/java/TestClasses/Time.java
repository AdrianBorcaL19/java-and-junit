package TestClasses;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Time {
    public void testTime(){
        LocalDateTime currentTime = LocalDateTime.now();
        System.out.println("Current TestClasses.Time: "+currentTime.format(DateTimeFormatter.ofPattern("hh:mm:ss a")));
        LocalDate currentDate = LocalDate.now();
        System.out.println("Current Date: "+currentDate);
        LocalTime time1 = LocalTime.of(10,32,30);
        System.out.println("TestClasses.Time 1:"+ time1);
        boolean leapYear = LocalDate.now().isLeapYear();
        System.out.print("Is "+ LocalDate.now().format(DateTimeFormatter.ofPattern("YYYY")) + " a leap year? " );
        System.out.println("Answer:"+leapYear);
    }
}
