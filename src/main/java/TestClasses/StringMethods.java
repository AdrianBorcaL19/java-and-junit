package TestClasses;

import java.util.List;
import java.util.stream.Collectors;

public class StringMethods {

    public String repeat(String s1,int value){
        return s1.repeat(value);
    }

    public List<String> parseNewLines(String s1) {
        return s1.lines().collect(Collectors.toList());
    }
    public boolean isStringBlank(String s1){
        return s1.isBlank();
    }
    public String removeBeginningWhitespace(String s1){
        return s1.stripLeading();
    }
    public String removeEndWhitespace(String s1){
        return s1.stripTrailing();
    }
    public String removeWhitespace(String s1){
        return s1.strip();
    }
}
