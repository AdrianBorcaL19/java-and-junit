package TestClasses;

import java.util.Optional;

public class OptionalTest {
    public void testOptional(){
        String name = "Adrian";
        Optional<String> opt = Optional.of(name);
        System.out.println(opt);
    }
    public void triggeringExceptionOptional(){
        try{
            String name2 = null;
            Optional<String> opt2 = Optional.of(name2);
            System.out.println(opt2);
        }catch(NullPointerException e){
            System.out.println("Cannot pass a null argument to .of");
        }
    }
    public void emptyOptional(){
        String name3 = null;
        Optional<String> opt3 = Optional.ofNullable(name3);
        System.out.println(opt3);
    }
}
