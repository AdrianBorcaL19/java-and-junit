package TestClasses;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class ReadFile {
    public String firstMethod(String filename) throws IOException {
        File file = new File(filename);
        Scanner scan = new Scanner(file);
        StringBuilder txt = new StringBuilder();
        while(scan.hasNextLine()){
            txt.append(scan.nextLine());
        }
        return String.valueOf(txt);
    }
    public String secondMethod(String filename) throws IOException {
        FileReader fileReader = new FileReader(filename);
        StringBuilder txt = new StringBuilder();
        while(true){
            int check = fileReader.read();
            if(check!=-1){
                txt.append((char) check);
            }else{
                break;
            }
        }
        return String.valueOf(txt);
    }
}
