package TestClasses;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class TryWithResources {
    public void testTryWithResources() {
        try(FileOutputStream fileOutputStream = new FileOutputStream("file.txt")){
            String text = "Try with resources";
            fileOutputStream.write(text.getBytes());
        }catch(FileNotFoundException e){
            System.out.println("File not found");
        }catch(IOException e){
            System.out.println("IO Exception");
        }
    }
}
