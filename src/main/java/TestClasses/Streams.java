package TestClasses;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
import java.util.stream.Collectors;

public class Streams {
    Integer[] testNumbers = {1, 2, 3, 4, 5, 6, 7, -3};
    public void arraytoList() {
        List<Integer> toList = Arrays.stream(testNumbers)
                .collect(Collectors.toList());
        System.out.println(toList);
    }
    public void collectGreaterThanFive() {
        List<Integer> greaterFive = Arrays.stream(testNumbers)
                .filter(x -> x > 5)
                .collect(Collectors.toList());
        System.out.println("Greater than five " + greaterFive);
    }
    public  List<Integer> collectEvenSquares(Integer[] input) {
        return Arrays.stream(input).
                map(x -> x * x)
                .filter(x -> x % 2 == 0)
                .collect(Collectors.toList());
    }
    public Integer findFirstDivisibleFive(Integer[] input) {
        return Arrays.stream(input)
                .filter(x-> x % 5== 0)
                .findFirst()
                .orElseThrow();
    }

    public void streamPeek() {
        List<Integer> toList =  Arrays.stream(testNumbers).collect(Collectors.toList());
        List<Integer> streamPeek = toList.stream().peek(x-> System.out.println("Before filter " + x))
                .filter(x-> x % 2 == 1 )
                .peek(x-> System.out.println( "After filter "+ x))
                .collect(Collectors.toList());
    }

       public Integer findMin(Integer[] input) {
          List<Integer> toList =  Arrays.stream(input).collect(Collectors.toList());
           return toList.stream().mapToInt(x -> x).min().getAsInt();
       }
       public Integer findMax(Integer[] input) {
           List<Integer> toList =  Arrays.stream(input).collect(Collectors.toList());
           return toList.stream().mapToInt(x-> x).max().getAsInt();
       }

}

