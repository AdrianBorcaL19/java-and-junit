package TestClasses;

import java.util.List;
import java.util.Map;
import java.util.Set;


import static java.util.Map.entry;

public class FactoryMethods {
    public void testFactoryMethods(){
        List<String> list1 = List.of("adrian","darius","timeea");
        Set<String> set1 = Set.of("timeea","darius","adrian");
        Map<String,String> map1 = Map.of("Borca","Adrian","Ghiura","Darius","Pitan","Timeea");
        Map<String,String> map2 = Map.ofEntries(
                entry("Borca","Adrian"),
                entry("Pitan","Timeea"),
                entry("Ghiura","Darius")
        );
    }
}
