package TestClasses;


import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class StringMethodsTest {
    private final StringMethods stringMethods = new StringMethods();
    @Test
    public void testRepeat() {
        String repeatInput = "repeat";
        int repeatNumber = 3;
        String repeatExpected = "repeatrepeatrepeat";
        String actual = stringMethods.repeat(repeatInput,repeatNumber);
        assertEquals(repeatExpected,actual);
    }

    @Test(expected = NullPointerException.class)
    public void testRepeatWithNull(){
        int repeatNumber = 3;
        String actual = stringMethods.repeat(null,repeatNumber);
        assertNull(actual);
    }

    @Test
    public void testIsStringBlankFalse() {
        String blankInput = "12345678910";
        boolean actual = stringMethods.isStringBlank(blankInput);
        assertFalse(actual);
    }

    @Test
    public void testRemoveBeginningWhitespace() {
        String beginningInput = "      adrian";
        String beginningExpected = "adrian";
        String actual = stringMethods.removeBeginningWhitespace(beginningInput);
        assertEquals(actual,beginningExpected);
    }

    @Test
    public void testRemoveEndWhitespace() {
        String endInput = "adrian   ";
        String endExpected = "adrian";
        String actual = stringMethods.removeEndWhitespace(endInput);
        assertEquals(actual,endExpected);
    }

    @Test
    public void testRemoveWhitespace() {
        String removeInput = "     adrian  ";
        String removeExpected = "adrian";
        String actual = stringMethods.removeWhitespace(removeInput);
        assertEquals(removeExpected,actual);
    }

    @Test
    public void parseNewLines() {
        String newLinesInput = "Adrian\nTimeea\nDarius";
        List<String> newLinesExpected = List.of("Adrian","Timeea","Darius");
        List<String> actual = stringMethods.parseNewLines(newLinesInput);
        assertEquals(actual,newLinesExpected);
    }
}