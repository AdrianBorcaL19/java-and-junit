package TestClasses;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class ReadFileTest {
    private final ReadFile readFile = new ReadFile();
    private final String correctFilename = "file.txt";
    private final String badFilename = "random";
    private final String expectedText = "ana are mere";

    @Test
    public void testReadFromFileFirstMethod() throws IOException {
        String actual = readFile.firstMethod(correctFilename);
        assertEquals(actual,expectedText);
    }
    @Test(expected = IOException.class)
    public void testReadFromFileFirstMethodBadFilename() throws IOException {
        String actual = readFile.firstMethod(badFilename);
        Assert.fail("Code should never reach here");
        assertEquals(actual,expectedText);
    }

    @Test
    public void testReadFromFileSecondMethod() throws IOException {
        String actual = readFile.secondMethod(correctFilename);
        assertEquals(actual,expectedText);
    }
    @Test(expected = IOException.class)
    public void testReadFromFileSecondMethodBadFilename() throws IOException {
        String actual = readFile.secondMethod(badFilename);
        Assert.fail("Code should never reach here");
        assertEquals(actual,expectedText);
    }
}