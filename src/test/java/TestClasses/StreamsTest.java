package TestClasses;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.NoSuchElementException;

import static org.junit.Assert.*;

public class StreamsTest {
    private final Streams streams = new Streams();
    @Test
    public void testCollectEvenSquares() {
        Integer[] inputList = {1,2,3,4,5,6,-10};
        List<Integer> expectedList = List.of(4,16,36,100);
        List<Integer> actual = streams.collectEvenSquares(inputList);
        assertEquals(actual,expectedList);
    }

    @Test
    public void testFindFirstDivisibleFive() {
        Integer[] inputList = {1,2,3,4,5,6,-10};
        Integer expectedOutput = 5;
        Integer actual = streams.findFirstDivisibleFive(inputList);
        assertEquals(actual,expectedOutput);
    }
    @Test(expected = NoSuchElementException.class)
    public void testFindFirstDivisibleElementNotFound() {
        Integer[] inputList = {1,2,3,4,9,6,3};
        Integer expectedOutput = null;
        Integer actual = streams.findFirstDivisibleFive(inputList);
        Assert.fail("Code should never reach here");
        assertEquals(actual,expectedOutput);
    }

    @Test
    public void testFindMin() {
        Integer[] inputList = {1,2,3,4,5,6,-10};
        Integer expectedOutput = -10;
        Integer actual = streams.findMin(inputList);
        assertEquals(actual,expectedOutput);
    }

    @Test
    public void testFindMax() {

        Integer[] inputList = {90,19,8,19,20,5};
        Integer expectedOutput = 90;
        Integer actual = streams.findMax(inputList);
        assertEquals(actual,expectedOutput);
    }
}